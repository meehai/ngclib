#!/usr/bin/env python3
from __future__ import annotations
from argparse import ArgumentParser, Namespace
from types import SimpleNamespace
from functools import partial
from typing import List, Callable
from pathlib import Path
from ngclib.graph_cfg import GraphCfg, NGCNodesImporter
from ngclib.models import NGCV1, NGCNode
from ngclib.models.edges import SingleLink
from ngclib.readers import NGCNpzReader
from ngclib.utils import generate_random_data
from ngclib.semisupervised import pseudo_algo
from torch import nn, optim
from torch.utils.data import DataLoader
from omegaconf import OmegaConf


class BaseNode(NGCNode):
    def load_from_disk(self, x): return x
    def save_to_disk(self, x): return x
class A(BaseNode):
    def __init__(self, name: str):
        super().__init__(name, dims=(1,))
class B(BaseNode):
    def __init__(self, name: str):
        super().__init__(name, dims=(2,))
class C(BaseNode):
    def __init__(self, name: str):
        super().__init__(name, dims=(3,))
class D(BaseNode):
    def __init__(self, name: str):
        super().__init__(name, dims=(4,))
class E(BaseNode):
    def __init__(self, name: str):
        super().__init__(name, dims=(5,))
class F(BaseNode):
    def __init__(self, name: str):
        super().__init__(name, dims=(6,))


class NGCSimple(NGCV1):
    @staticmethod
    def build_from_cfg(nodes: List[NGCNode], graph_cfg: GraphCfg, vote_fn: Callable) -> NGCSimple:
        assert graph_cfg.ngc_type == NGCSimple, graph_cfg.ngc_type
        name_to_node = {node.name: node for node in nodes}
        input_nodes: List[NGCNode] = [v for k, v in name_to_node.items() if k in graph_cfg.input_nodes]

        edges = []
        for edge in graph_cfg.edges_raw["SL"]:
            assert len(edge) == 2, edge
            node_in, node_out = name_to_node[edge[0]], name_to_node[edge[1]]
            edges.append(SingleLink(node_in, node_out, None, lambda _in, out: nn.Linear(_in, out)))
        return NGCSimple(edges, vote_fn, input_nodes)

def main(args: Namespace):
    generate_random_data(
        args.data_path,
        names=["a", "b", "c", "d", "e", "f"],
        dims=[1, 2, 3, 4, 5, 6],
        types="float",
        shape=[5, 10],
        num_items=[10, 10, 10, 5, 4, 7],
    )
    graph_cfg = GraphCfg(OmegaConf.to_container(OmegaConf.load(args.config_path)), ngc_type=NGCSimple)
    vote_fn = lambda votes, sources: votes.mean(axis=1)

    node_str_to_type = {"A": A, "B": B, "C": C, "D": D, "E": E, "F": F}
    nodes = NGCNodesImporter.from_graph_cfg(graph_cfg, node_str_to_type=node_str_to_type).nodes
    graph: NGCSimple = NGCSimple.build_from_cfg(nodes, graph_cfg, vote_fn)

    try:
        _ = NGCNpzReader(args.data_path, graph.nodes, graph.output_nodes)
        raise ValueError
    except AssertionError:
        pass

    pseudo_algo(graph, args.data_path, args.out_path)
    reader = NGCNpzReader(args.out_path, graph.nodes, graph.output_nodes)
    _ = DataLoader(reader, collate_fn=reader.collate_fn, batch_size=2)

def test_run_pseudo():
    args = SimpleNamespace(**{
        "config_path": Path(__file__).parent / "ngc_v1.yaml",
        "data_path": Path("/tmp/ngc_pseudolabels_example/data"),
        "out_path": Path("/tmp/ngc_pseudolabels_example/output"),
    })
    main(args)

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--config_path", type=lambda p: Path(p).absolute(), default="ngc_v1.yaml")
    parser.add_argument("--data_path", type=lambda p: Path(p).absolute(), default="/tmp/ngc_pseudolabels_example/data")
    parser.add_argument("-o", "--out_path", type=lambda p: Path(p).absolute(),
                        default="/tmp/ngc_pseudolabels_example/output")
    args = parser.parse_args()
    main(args)
