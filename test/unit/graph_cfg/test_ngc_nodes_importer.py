import torch as tr
from pathlib import Path
from ngclib.graph_cfg import GraphCfg, NGCNodesImporter
from ngclib.models import NGCEdge, NGCNode
from torch import nn
import yaml


class RGB(NGCNode): pass
class Semantic(NGCNode): pass
class Depth(NGCNode): pass

def test_nodes_importer_1():
    graph_cfg = GraphCfg(yaml.safe_load(open(Path(__file__).parent / "test_graph_v1.yaml", "r")))
    nodes = NGCNodesImporter.from_graph_cfg(
        graph_cfg, node_str_to_type={"RGB": RGB, "Semantic": Semantic, "Depth": Depth}
    ).nodes
    name_to_node = {node.name: node for node in nodes}
    assert len(nodes) == 3
    assert sorted(list(name_to_node.keys())) == ["depth", "rgb", "semantic"]


def test_nodes_importer_and_edge_forward_1():
    graph_cfg = GraphCfg(yaml.safe_load(open(Path(__file__).parent / "test_graph_v1.yaml", "r")))
    nodes = NGCNodesImporter.from_graph_cfg(
        graph_cfg, node_str_to_type={"RGB": RGB, "Semantic": Semantic, "Depth": Depth}
    ).nodes
    name_to_node = {node.name: node for node in nodes}
    edge_1 = NGCEdge(name_to_node["rgb"], name_to_node["depth"], "RGB -> Depth", nn.Linear)
    edge_2 = NGCEdge(name_to_node["rgb"], name_to_node["semantic"], "RGB -> Semantic", nn.Linear)
    x = {
        "rgb": tr.randn(10, 3),
    }
    y1 = edge_1.forward(x["rgb"])
    y2 = edge_2.forward(x["rgb"])
    assert y1.shape == (10, 1) and y2.shape == (10, 12)

if __name__ == "__main__":
    test_nodes_importer_and_edge_forward_1()
