import yaml
from ngclib.graph_cfg import GraphCfg
from ngclib.models import NGC, NGCV1, NGCEnsemble, NGCHyperEdges
from pathlib import Path

cwd = Path(__file__).absolute().parent

def test_graph_cfg_ngc_type_1():
    possible_types = {"NGC-V1": NGCV1, "NGC-Ensemble": NGCEnsemble, "NGC-HyperEdges": NGCHyperEdges}
    for type_str, _type in possible_types.items():
        fake_cfg = {
            "NGC-Architecture": type_str,
            "nodes": {"types": ["A"], "names": ["A"], "inputNodes": ["A"]},
            "edges": {},
        }
        graph_cfg = GraphCfg(fake_cfg)
        assert graph_cfg.ngc_type == _type, type_str


def test_graph_cfg_ngc_type_2():
    class MyNGC(NGC):
        pass

    fake_cfg = {"NGC-Architecture": MyNGC, "nodes": {"types": ["A"], "names": ["A"], "inputNodes": ["A"]}}
    try:
        _ = GraphCfg(fake_cfg, ngc_type=MyNGC)
    except AssertionError:
        pass


def test_graph_cfg_ngc_type_3():
    class MyNGC(NGC):
        pass

    fake_cfg = {"nodes": {"types": ["A"], "names": ["A"], "inputNodes": ["A"]}}
    graph_cfg = GraphCfg(fake_cfg, ngc_type=MyNGC)
    assert graph_cfg.ngc_type == MyNGC

def test_graph_cfg_hparams_ok():
    fake_cfg = yaml.safe_load(
"""NGC-Architecture: NGC-V1
nodes:
  types: [RGB, HSV, Semantic, SoftEdges, Depth, Normals]
  names: [rgb, hsv, segprop8, softedges, depthSfm, cameraNormalsSfm]
  inputNodes: [rgb]
hyperParameters:
  depthSfm:
    max_depth_meters: 1

  segprop8:
    semantic_classes: [land, forest, residential, road, little-objects, water, sky, hill]
    semantic_colors: [[0, 255, 0], [0, 127, 0], [255, 255, 0], [255, 255, 255],
                      [255, 0, 0], [0, 0, 255], [0, 255, 255], [127, 127, 63]]
    one_hot_encoding: True

  cameraNormalsSfm:
    representation: euler
""")
    _ = GraphCfg(fake_cfg)

def test_graph_cfg_too_many_hparams():
    fake_cfg = yaml.safe_load(
"""NGC-Architecture: NGC-V1
nodes:
  types: [RGB, HSV, Semantic, SoftEdges, Depth]
  names: [rgb, hsv, segprop8, softedges, depthSfm]
  inputNodes: [rgb]
hyperParameters:
  depthSfm:
    max_depth_meters: 1

  segprop8:
    semantic_classes: [land, forest, residential, road, little-objects, water, sky, hill]
    semantic_colors: [[0, 255, 0], [0, 127, 0], [255, 255, 0], [255, 255, 255],
                      [255, 0, 0], [0, 0, 255], [0, 255, 255], [127, 127, 63]]
    one_hot_encoding: True

  cameraNormalsSfm:
    representation: euler
""")

    try:
        _ = GraphCfg(fake_cfg)
        raise Exception
    except AssertionError:
        pass

def test_graph_cfg_too_many_hparams():
    fake_cfg = yaml.safe_load(
"""NGC-Architecture: NGC-V1
nodes:
  types: [RGB, HSV, Semantic, SoftEdges, Depth]
  names: [rgb, hsv, segprop8, softedges, depthSfm]
  inputNodes: [rgb]
hyperParameters:
  depthSfm:
    max_depth_meters: 1

  segprop8:
    semantic_classes: [land, forest, residential, road, little-objects, water, sky, hill]
    semantic_colors: [[0, 255, 0], [0, 127, 0], [255, 255, 0], [255, 255, 255],
                      [255, 0, 0], [0, 0, 255], [0, 255, 255], [127, 127, 63]]
    one_hot_encoding: True

  cameraNormalsSfm:
    representation: euler
""")

    try:
        _ = GraphCfg(fake_cfg)
        raise Exception
    except AssertionError:
        pass

if __name__ == "__main__":
    test_graph_cfg_properties_hyperedges_1()
