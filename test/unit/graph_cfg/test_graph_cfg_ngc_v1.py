import yaml
from ngclib.graph_cfg import GraphCfg
from ngclib.models import NGC, NGCV1
from pathlib import Path

cwd = Path(__file__).absolute().parent

def test_graph_cfg_ngc_v1_ngc_type():
    graph_cfg = GraphCfg(yaml.safe_load(open(cwd / "test_graph_v1.yaml", "r")))
    assert graph_cfg.ngc_type == NGCV1

def test_graph_cfg_ngc_v1_properties_1():
    graph_cfg = GraphCfg(yaml.safe_load(open(cwd / "test_graph_v1.yaml", "r")))
    assert graph_cfg.node_names == ["rgb", "semantic", "depth"]
    assert graph_cfg.node_types == ["RGB", "Semantic", "Depth"]
    assert graph_cfg.input_nodes == ["rgb"]
    assert graph_cfg.node_args == {"rgb": {"dims": [3]}, "semantic": {"dims": [12]}, "depth": {"dims": [1]}}
    assert graph_cfg.output_nodes == ["depth", "semantic"]
    assert graph_cfg.seed == 42

def test_graph_cfg_ngc_v1_properties_2():
    graph_cfg = GraphCfg(yaml.safe_load(open(cwd / "test_graph_v1.yaml", "r")))
    assert graph_cfg.edges_raw == {"SL": [["rgb", "semantic"], ["rgb", "depth"]], "TH": [["rgb", "depth", "semantic"]]}
    assert graph_cfg.edges == [
        "Single Link rgb -> semantic",
        "Single Link rgb -> depth",
        "TwoHop Link (rgb ->) depth -> semantic",
    ]

