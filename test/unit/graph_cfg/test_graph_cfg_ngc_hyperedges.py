import yaml
from ngclib.graph_cfg import GraphCfg
from ngclib.models import NGC, NGCV1, NGCEnsemble, NGCHyperEdges
from pathlib import Path

cwd = Path(__file__).absolute().parent

def test_graph_cfg_properties_hyperedges_1():
    graph_cfg = GraphCfg(yaml.safe_load(open(cwd / "test_graph_hyperedges.yaml", "r")))
    assert graph_cfg.edges_raw == {
        "H1": [
            ["*", "AOD"],
            ["*", "CarbonMonoxide"],
            ["*", "FIRE"],
            ["*", "LAI"],
            ["*", "LSTD_AN"],
            ["*", "LSTN_AN"],
            ["*", "WV"],
        ]
    }

    assert graph_cfg.edges == [
        "H1 [CHLORA,CLD_FR,CLD_RD,CLD_WP,COT,LSTD,LSTN,NO2,NVDI,OZONE,SNOWC,SST] -> AOD",
        "H1 [CHLORA,CLD_FR,CLD_RD,CLD_WP,COT,LSTD,LSTN,NO2,NVDI,OZONE,SNOWC,SST] -> CarbonMonoxide",
        "H1 [CHLORA,CLD_FR,CLD_RD,CLD_WP,COT,LSTD,LSTN,NO2,NVDI,OZONE,SNOWC,SST] -> FIRE",
        "H1 [CHLORA,CLD_FR,CLD_RD,CLD_WP,COT,LSTD,LSTN,NO2,NVDI,OZONE,SNOWC,SST] -> LAI",
        "H1 [CHLORA,CLD_FR,CLD_RD,CLD_WP,COT,LSTD,LSTN,NO2,NVDI,OZONE,SNOWC,SST] -> LSTD_AN",
        "H1 [CHLORA,CLD_FR,CLD_RD,CLD_WP,COT,LSTD,LSTN,NO2,NVDI,OZONE,SNOWC,SST] -> LSTN_AN",
        "H1 [CHLORA,CLD_FR,CLD_RD,CLD_WP,COT,LSTD,LSTN,NO2,NVDI,OZONE,SNOWC,SST] -> WV",
    ]