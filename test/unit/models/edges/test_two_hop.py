from torch import nn
from ngclib.models import NGCNode
from ngclib.models.edges import TwoHopLink

def test_two_hop_link_1():
    edge = TwoHopLink(NGCNode("a", (1, )), NGCNode("b", (2, )), NGCNode("c", (3, )), None, nn.Linear)
    assert edge is not None