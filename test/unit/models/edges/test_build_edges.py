from ngclib.graph_cfg import NGCNodesImporter
from ngclib.models import NGCNode, NGCV1, NGCEnsemble, NGCHyperEdges
from ngclib.models.edges import SingleLink, EnsembleEdge, HyperEdge
from torch import nn
import torch as tr

class RGB(NGCNode): pass
class Semantic(NGCNode): pass
class Depth(NGCNode): pass
MB = 10

def _get_nodes():
    nodes = NGCNodesImporter(node_names=["rgb_epic", "hsv_narrative", "depth_lyric", "semantic_dramatic"],
                             node_types=["RGB", "HSV", "Depth", "Semantic"],
                             node_args={"rgb_epic": {"dims": (99, )},
                                        "hsv_narrative": {"dims": (55, )},
                                        "depth_lyric": {"dims": (1, )},
                                        "semantic_dramatic": {"dims": (4200, )},
                                       },
                             node_str_to_type={"RGB": RGB, "Semantic": Semantic, "Depth": Depth, "HSV": RGB},
                             ).nodes
    vote_fn = lambda votes, sources: votes.mean(axis=1)
    model_fn = nn.Linear
    input_node_names = ["rgb_epic", "hsv_narrative"]
    return nodes, vote_fn, model_fn, input_node_names


def test_build_edges_ngc_v1_1():
    nodes, vote_fn, model_fn, input_node_names = _get_nodes()
    edges_str = {
        "SL": [
            ["rgb_epic", "depth_lyric"],
            ["rgb_epic", "semantic_dramatic"],
        ],
        "TH": [
            ["rgb_epic", "depth_lyric", "semantic_dramatic"],
        ]
    }

    edges = NGCV1.build_graph_from_edges_cfg(nodes, edges_str, model_fn, vote_fn, input_node_names).edges
    assert len(edges) == 3

    x = tr.randn(MB, 99)
    y1 = edges[0].forward(x)
    y2 = edges[1].forward(x)
    y3 = edges[2].forward(y1)
    assert y1.shape == (MB, 1) and y2.shape == (MB, 4200) and y3.shape == (MB, 4200)


def test_build_edges_ngc_ensemble_1():
    nodes, vote_fn, model_fn, input_node_names = _get_nodes()
    edges_str = {
        "SL": [
            ["rgb_epic", "depth_lyric"],
            ["rgb_epic", "semantic_dramatic"],
            ["hsv_narrative", "depth_lyric"],
        ],
        "ENS": [
            ["depth_lyric", "semantic_dramatic"],
            ["semantic_dramatic", "depth_lyric"],
        ]
    }
    edges = NGCEnsemble.build_graph_from_edges_cfg(nodes, edges_str, model_fn, vote_fn, input_node_names).edges
    assert len(edges) == 5, len(edges)
    x0 = tr.randn(MB, 99)
    x1 = tr.randn(MB, 55)

    y0 = edges[0].forward(x0)
    y1 = edges[1].forward(x0)
    y2 = edges[2].forward(x1)

    x02 = vote_fn(tr.stack([y0, y2], axis=1), [edges[0], edges[2]])
    y3 = edges[3].forward(x02)

    y4 = edges[4].forward(y1)

    assert y0.shape == (MB, 1) and y1.shape == (MB, 4200) and y2.shape == (MB, 1) and \
           y3.shape == (MB, 4200) and y4.shape == (MB, 1)

def test_build_edges_ngc_hyperedges_1():
    nodes, vote_fn, model_fn, input_node_names = _get_nodes()
    edges_str = {
        "SL": [
            ["rgb_epic", "depth_lyric"],
            ["rgb_epic", "semantic_dramatic"],
            ["hsv_narrative", "depth_lyric"],
        ],
        "ENS": [
            ["depth_lyric", "semantic_dramatic"],
            ["semantic_dramatic", "depth_lyric"],
        ],
        "H1": [
            ["*", "semantic_dramatic"],
            [ ["rgb_epic", "hsv_narrative"], "depth_lyric"],
        ]
    }
    edges = NGCHyperEdges.build_edges(nodes, edges_str, model_fn, input_node_names)

    sls = [e for e in edges if isinstance(e, SingleLink)]
    ensembles = [e for e in edges if isinstance(e, EnsembleEdge)]
    hypers = [e for e in edges if isinstance(e, HyperEdge)]
    assert len(sls) == 3
    assert len(ensembles) == 2
    assert len(hypers) == 2
    assert len(edges) == 11
    # there are 2 duplicate edges computed from cfg. These are automatically removed in Graph.
    model = NGCHyperEdges.build_graph_from_edges_cfg(nodes, edges_str, model_fn, vote_fn, input_node_names)
    assert len(model.edges) == 9

if __name__ == "__main__":
    test_build_edges_ngc_ensemble_1()
