import torch as tr
from typing import Dict
from nwgraph.node import CatNode
from ngclib.models import NGCNode, NGCHyperEdges
from ngclib.graph_cfg import NGCNodesImporter
from torch import nn

class InNode(NGCNode): pass
class OutNode(NGCNode): pass

def _get_nodes(nodes: Dict):
    input_node_names = [n for n in nodes.keys() if n.startswith("in")]

    nodes = NGCNodesImporter(node_names=list(nodes.keys()),
                             node_types=["InNode" if node in input_node_names else "OutNode" for node in nodes.keys()],
                             node_args={node_name: {"dims": (v, )} for node_name, v in nodes.items()},
                             node_str_to_type={"InNode": InNode, "OutNode": OutNode},
                             ).nodes
    vote_fn = lambda votes, sources: votes.mean(axis=1)
    model_fn = nn.Linear
    return nodes, vote_fn, model_fn, input_node_names

def test_ngc_hyperedges_h1_1():
    MB = 100
    nodes_def = {"in1": 1, "in2": 2, "in3": 5, "in4": 10, "in5": 3, "out1": 10}
    nodes, vote_fn, model_fn, input_node_names = _get_nodes(nodes_def)
    edges_str = {
        "H1": [
            ["*", "out1"],
        ]
    }
    ngc = NGCHyperEdges.build_graph_from_edges_cfg(nodes, edges_str, model_fn, vote_fn, input_node_names)
    x = {node.name: tr.randn(MB, *node.dims) for node in ngc.input_nodes}
    y = ngc.forward(x)
    for node in nodes:
        assert y[node].shape == (MB, *node.dims)
    for input_node in input_node_names:
        assert tr.allclose(y[input_node], x[input_node])

def test_ngc_hyperedges_h1_order_check():
    MB = 100
    nodes_def = {"in1": 1, "in2": 2, "in3": 5, "in4": 10, "in5": 3, "out1": 10}
    nodes, vote_fn, model_fn, input_node_names = _get_nodes(nodes_def)
    edges_str = {
        "H1": [
            ["*", "out1"],
        ]
    }
    ngc = NGCHyperEdges.build_graph_from_edges_cfg(nodes, edges_str, model_fn, vote_fn, input_node_names)
    x = {node.name: tr.randn(MB, *node.dims) for node in ngc.input_nodes}
    _ = ngc.forward(x)
    # Order check
    cat_nodes = [n for n in ngc.nodes if isinstance(n, CatNode)]
    assert len(cat_nodes) == 1, cat_nodes
    cat_node = cat_nodes[0]
    assert [x.name for x in cat_node.nodes] == sorted(cat_node.nodes, key=lambda n: n.name), cat_node.nodes

    out_node = nodes[-1]
    assert len(out_node.messages) == 1
    # get the message H1[in1,in2,in3,in4,in5] -> out1 and check order of concatenation
    out_edge_in_msg = out_node.messages[0].path[ngc.edges[-1]]
    assert out_edge_in_msg.shape == (MB, sum(n.dims[-1] for n in ngc.input_nodes))

    # Take the slice from the last dimension to deconstruct the concatenation for each input node
    current = 0
    for input_node in ngc.input_nodes:
        x_from_message = out_edge_in_msg[..., current: current + input_node.dims[-1]]
        x_from_data = x[input_node]
        assert tr.allclose(x_from_message, x_from_data), (ngc.input_nodes, out_edge_in_msg)
        current += input_node.dims[-1]

def test_ngc_hyperedges_h2_1():
    MB = 100
    nodes_def = {"in1": 1, "in2": 2, "in3": 5, "in4": 10, "in5": 3, "out1": 10, "out2": 15, "out3": 7}
    nodes, vote_fn, model_fn, input_node_names = _get_nodes(nodes_def)
    edges_str = {
        "H1": [
            ["*", "out1"],
            ["*", "out2"],
            ["*", "out3"],
        ],
        "H2R": [
            ["*", "out1"]
        ]
    }
    ngc = NGCHyperEdges.build_graph_from_edges_cfg(nodes, edges_str, model_fn, vote_fn, input_node_names)
    x = {node.name: tr.randn(MB, *node.dims) for node in ngc.input_nodes}
    y = ngc.forward(x)
    for node in nodes:
        assert y[node].shape == (MB, *node.dims)
    for input_node in input_node_names:
        assert tr.allclose(y[input_node], x[input_node])
    out1, out2, out3 = nodes[-3: ]
    assert len(out3.messages) == 1
    assert len(out2.messages) == 1
    assert len(out1.messages) == 2
    assert tr.allclose(y[out1], (out1.messages[0].content + out1.messages[1].content) / 2)

if __name__ == "__main__":
    test_ngc_hyperedges_h1_1()
