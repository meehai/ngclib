import torch as tr
from ngclib.models import NGCV1, NGCNode
from ngclib.models.edges import SingleLink, TwoHopLink

def test_ngcv1_message_pass_1():
    MB = 10
    nodes = {
        "a": NGCNode("a", (1, )), "b": NGCNode("b", (2, )), "c": NGCNode("c", (3, ))
    }
    sl = SingleLink(nodes["a"], nodes["b"], None, lambda d_in, d_out: tr.nn.Linear(d_in, d_out))
    th = TwoHopLink(nodes["a"], nodes["b"], nodes["c"], None, lambda d_in, d_out: tr.nn.Linear(d_in, d_out))
    vote_fn = lambda votes, sources: votes.mean(axis=1)
    graph = NGCV1([sl, th], vote_fn, ["a"])
    x = {"a": tr.randn(MB, 1)}
    y = graph.forward(x)
    assert tr.allclose(y["a"], x["a"])
    assert y["b"].shape == (MB, 2)
    assert y["c"].shape == (MB, 3)

def test_two_hop_link_message_pass_2():
    MB = 10
    nodes = {
        "a": NGCNode("a", (1, )), "b": NGCNode("b", (2, )), "c": NGCNode("c", (3, )), "d": NGCNode("d", (4, ))
    }
    sl = SingleLink(nodes["a"], nodes["b"], None, lambda d_in, d_out: tr.nn.Linear(d_in, d_out))
    th = TwoHopLink(nodes["d"], nodes["b"], nodes["c"], None, lambda d_in, d_out: tr.nn.Linear(d_in, d_out))
    vote_fn = lambda votes, sources: votes.mean(axis=1)
    try:
        _ = NGCV1([sl, th], vote_fn, ["a"])
    except AssertionError:
        pass
