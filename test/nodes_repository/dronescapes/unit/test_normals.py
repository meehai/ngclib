from nodes_repository.dronescapes import Normals


def test_node_normals_1():
    node: Normals = Normals("normals", representation="euler")
    assert not node is None
    assert node.name == "normals"
    assert node.representation == "euler"
    assert node.num_channels == 3
