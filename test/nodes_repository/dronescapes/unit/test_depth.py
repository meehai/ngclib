from nodes_repository.dronescapes import Depth


def test_node_depth_1():
    node = Depth("depth", max_depth_meters=300)
    assert not node is None
    assert node.name == "depth"
    assert node.max_depth_meters == 300
    assert node.num_channels == 1


if __name__ == "__main__":
    test_node_depth_1()
