from nodes_repository.dronescapes import SoftSegmentation


def test_node_hsv_1():
    node = SoftSegmentation("soft_segmentation")
    assert not node is None
    assert node.name == "soft_segmentation"
    assert node.num_channels == 3
