from nodes_repository.dronescapes import Edges


def test_node_edges_1():
    node = Edges("edges")
    assert not node is None
    assert node.name == "edges"
    assert node.num_channels == 2
