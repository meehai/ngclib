#!/usr/bin/env python3
import torch as tr
from argparse import ArgumentParser
from pathlib import Path
from ngclib.trainer.async_trainer import NGCAsyncTrainer
from lightning_module_enhanced import LME
from lightning_module_enhanced.callbacks import PlotCallback
from omegaconf import OmegaConf
from ngclib.graph_cfg import GraphCfg
from nodes_repository.dronescapes import DronescapesNodesImporter
from ngclib.utils import get_library_root
from ngclib.readers import NGCNpzReader
from ngclib.logger import logger
from ngclib.semisupervised import make_iter1_data, pseudo_algo
from torch import nn
from media_processing_lib.collage_maker import collage_fn
from media_processing_lib.image import image_write

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def plot_fn(x, y, gt, out_dir, model):
    node_in, node_out = model.base_model.base_graph.edges[-1].nodes
    x_img = [node_in.plot_fn(_x) for _x in y[node_in.name]]
    y_img = [node_out.plot_fn(_y) for _y in y[node_out.name]]
    gt_img = [node_out.plot_fn(_gt) for _gt in gt[node_out.name]]
    for i, (x, y, gt) in enumerate(zip(x_img, y_img, gt_img)):
        collage = collage_fn([x, y, gt], rows_cols=(1, 3), titles=[node_in.name, node_out.name, "gt"])
        image_write(collage, out_dir / f"{i}.png")

def get_args():
    parser = ArgumentParser()
    parser.add_argument("config_path", type=Path)
    parser.add_argument("--dataset_path", type=Path, default=get_library_root() / "resources/dronescapes_min")
    parser.add_argument("--parallelize", action="store_true")
    args = parser.parse_args()
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    graph_cfg = GraphCfg(OmegaConf.to_container(cfg.graph))
    nodes = DronescapesNodesImporter.from_graph_cfg(graph_cfg).nodes
    ngc = graph_cfg.build_model(nodes, nn.Linear, lambda votes, sources: votes.mean(axis=1))
    print(LME(ngc).summary)
    ngc_dir_path = Path(__file__).parent.absolute() / args.config_path.stem
    ngc.to_graphviz().render(ngc_dir_path / "graph", cleanup=True, format="png")
    logger.info(f"NGC Dir: '{ngc_dir_path}'")

    semisupervised_iterations = 3
    val_reader = NGCNpzReader(args.dataset_path / "validation_set", nodes=ngc.nodes, out_nodes=ngc.output_nodes)
    trainer_params = {**OmegaConf.to_container(cfg.train.trainer_params), "callbacks": [PlotCallback(plot_fn)]}
    for it in range(1, semisupervised_iterations + 1):
        if it > 1:
            ngc.load_all_weights(ngc_dir_path / f"iter{it-1}/models")
            pseudo_algo(ngc, args.dataset_path / "validation_set", ngc_dir_path / f"iter{it}/data",
                        overwrite_potential_gt=False)

        make_iter1_data(args.dataset_path / "train_set", ngc_dir_path / f"iter{it}/data", graph_cfg.node_names)
        train_reader = NGCNpzReader(ngc_dir_path / f"iter{it}/data", nodes=ngc.nodes, out_nodes=ngc.output_nodes)

        trainer = NGCAsyncTrainer(
            graph=ngc,
            train_cfg=OmegaConf.to_container(cfg.train),
            train_reader=train_reader,
            validation_reader=val_reader,
            iter_dir=ngc_dir_path / f"iter{it}/models",
            dataloader_params=OmegaConf.to_container(cfg.data.loader_params),
            trainer_params=trainer_params,
            strategy = "parallel" if args.parallelize else "sequential",
            seed=cfg.seed,
        )
        trainer()

if __name__ == "__main__":
    main()
