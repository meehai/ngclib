#!/usr/bin/env python3
from argparse import ArgumentParser
from typing import List, Union, Dict, Callable
from pathlib import Path
import os
import numpy as np
import torch as tr
from omegaconf import OmegaConf
from torch import nn
from torch.nn import functional as F
from torchmetrics.functional.classification import accuracy
from torchmetrics.functional.regression.mae import mean_absolute_error

from ngclib.logger import logger
from ngclib.utils import generate_random_data
from ngclib.trainer.async_trainer import NGCAsyncTrainer
from ngclib.graph_cfg import GraphCfg, NGCNodesImporter
from ngclib.readers import NGCNpzReader
from ngclib.semisupervised import pseudo_algo, make_iter1_data
from ngclib.models import NGCNode

def generate_train_val_semisup():
    # Generate data
    base_dir = (Path(__file__).parent / "tmp_dataset").absolute()
    print(f"Generating random data at '{base_dir}'")
    representations = ["rgb", "hsv", "depth", "semantic"]
    dims = [3, 3, 1, 12]
    types = ["float", "float", "float", "categorical"]
    generate_random_data(base_dir / "train", representations, dims, types, (240, 426), 80, prefix="train_")
    generate_random_data(base_dir / "validation", representations, dims, types, (240, 426), 20)
    generate_random_data(base_dir / "semisupervised", representations, dims, types, (240, 426), 50)
    # remove 20 random ones from semisup dir. This means that we have _some_ GT, just not all.
    for r in ["depth", "semantic"]:
        r_items = [x for x in (base_dir / "semisupervised" / r).iterdir()]
        if len(r_items) != 50:
            continue
        items = np.random.choice(r_items, 20, replace=False)
        for item in items:
            os.unlink(item)
    return base_dir

class BaseNode(NGCNode):
    def save_to_disk(self, x): return x
    def load_from_disk(self, x):  return x

class RGB(BaseNode):
    def __init__(self, name: str):
        super().__init__(name, dims=(3, ))

class Depth(BaseNode):
    def __init__(self, name: str):
        super().__init__(name, dims=(1, ))

    def criterion_fn(self, y: tr.Tensor, gt: tr.Tensor) -> tr.Tensor:
        return (y - gt).abs().mean()

    @property
    def metrics(self) -> Dict[str, Callable]:
        return {"l1": (mean_absolute_error, "min")}

class Semantic(BaseNode):
    def __init__(self, name: str, classes: Union[int, List]):
        if isinstance(classes, list):
            num_classes = len(classes)
        else:
            num_classes = classes
            classes = range(num_classes)
        self.classes = classes
        super().__init__(name, dims=(num_classes, ))

    def criterion_fn(self, y: tr.Tensor, gt: tr.Tensor) -> tr.Tensor:
        assert len(self.messages) == 1
        y = y.reshape(-1, self.dims[-1])
        gt = gt.reshape(-1, self.dims[-1])
        return F.cross_entropy(y, gt)

    @property
    def metrics(self):
        def my_acc(y: tr.Tensor, gt: tr.Tensor):
            return accuracy(y.argmax(-1), gt.argmax(-1), task="multiclass", average="none", num_classes=self.dims[-1])
        return {"accuracy": (my_acc, "max")}

    def load_from_disk(self, x: np.ndarray) -> np.ndarray:
        return np.eye(self.dims[-1])[x]

    def save_to_disk(self, x: np.ndarray) -> np.ndarray:
        return x.argmax(-1).astype(np.uint8)

class HSV(RGB): pass
class ClassificationModel(nn.Linear): pass
class RegressionModel(nn.Linear): pass

def get_model_type(graph_cfg: GraphCfg) -> Dict:
    res = {}
    for edge_name, edge_raw_name in graph_cfg.edge_name_to_edge_raw.items():
        if edge_raw_name[-1] == "semantic":
            res[edge_name] = ClassificationModel
        else:
            res[edge_name] = RegressionModel
    return res

def get_args():
    parser = ArgumentParser()
    parser.add_argument("config_path", type=Path)
    parser.add_argument("--ngc_dir_path", type=Path, required=False, help="Defaults to --config_path stem.")
    parser.add_argument("--parallelize", action="store_true")
    args = parser.parse_args()
    if args.ngc_dir_path is None:
        args.ngc_dir_path = Path(__file__).parent.absolute() / args.config_path.stem
        logger.warning(f"--ngc_dir_path not provided. Defaulting to '{args.ngc_dir_path}'")
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    graph_cfg = GraphCfg(OmegaConf.to_container(cfg.graph))

    base_dir = generate_train_val_semisup()
    vote_fn = lambda votes, sources: votes.mean(axis=1)

    node_str_to_type = {"RGB": RGB, "HSV": HSV, "Depth": Depth, "Semantic": Semantic}
    nodes = NGCNodesImporter.from_graph_cfg(graph_cfg, node_str_to_type=node_str_to_type).nodes
    ngc = graph_cfg.build_model(nodes, get_model_type(graph_cfg), vote_fn)
    ngc.to_graphviz().render(args.ngc_dir_path / "graph", cleanup=True, format="png")
    print(ngc)
    validation_reader = NGCNpzReader(base_dir / "validation", ngc.nodes, ngc.output_nodes)

    iters = [1, 2, 3]
    for iter in iters:
        # pseudos for iter 2 and 3 only
        if iter > 1:
            ngc.load_all_weights(args.ngc_dir_path / f"iter{iter-1}/models")
            pseudo_algo(ngc, base_dir / "semisupervised", args.ngc_dir_path / f"iter{iter}/data")
        if iter == 3:
            break

        # Prepare data
        make_iter1_data(base_dir / "train", args.ngc_dir_path / f"iter{iter}/data", graph_cfg.node_names)
        train_reader = NGCNpzReader(args.ngc_dir_path / f"iter{iter}/data", ngc.nodes, ngc.output_nodes)

        # Train all the edges
        iter_dir = args.ngc_dir_path / f"iter{iter}/models"
        trainer = NGCAsyncTrainer(
            graph=ngc,
            train_cfg=OmegaConf.to_container(cfg.train),
            train_reader=train_reader,
            validation_reader=validation_reader,
            iter_dir=iter_dir,
            dataloader_params=OmegaConf.to_container(cfg.data.loader_params),
            trainer_params=OmegaConf.to_container(cfg.train.trainer_params),
            strategy = "parallel" if args.parallelize else "sequential",
            seed=cfg.seed,
        )
        trainer()

if __name__ == "__main__":
    main()
