#!/usr/bin/env python3
"""Downloads resources used in ngclib examples and some tests"""
from pathlib import Path
import subprocess
import os
import gdown
from ngclib.logger import logger

def main():
    """Download nasa_min and dronescapes_min"""
    dronescapes_min_url = "https://drive.google.com/uc?id=158zPUGa1XbtqMAXRUJocuFyB3Hj0xIiV"
    nasa_min_url = "https://drive.google.com/uc?id=1hQ8EofrrN0HqpCtz8opTvQyjQr-FV5lU"
    cwd = os.getcwd()

    def do_download(url, dataset: str):
        local_dir = (Path(__file__).parent / dataset).absolute()
        if local_dir.exists():
            logger.info(f"'{dataset}' already exists")
            return
        logger.info(f"Downloading '{dataset}' to '{local_dir}'")
        local_dir.mkdir(exist_ok=True, parents=True)
        gdown.download(url, f"{local_dir}/{dataset}.tar.gz")
        os.chdir(local_dir)
        subprocess.call(["tar", "-xzvf", f"{dataset}.tar.gz"])
        os.chdir(cwd)

    do_download(dronescapes_min_url, "dronescapes_min")
    do_download(nasa_min_url, "nasa_min")

if __name__ == "__main__":
    main()
