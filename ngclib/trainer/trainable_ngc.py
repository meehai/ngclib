"""Training graph module"""
from typing import List, Dict, Callable
from functools import partial
from overrides import overrides
from lightning_module_enhanced import TrainableModule, LME, TrainSetup
from lightning_module_enhanced.callbacks import PlotMetrics, RemovePreviousTrainArtifacts, CopyBestCheckpoint
from lightning_module_enhanced.metrics import CoreMetric, CallableCoreMetric
from torch import optim
from pytorch_lightning.callbacks import ModelCheckpoint, Callback
import torch as tr

from .callbacks import PlotGraph
from ..logger import logger
from ..models import NGC, NGCNode, NGCEdge


class TrainableNGC(TrainableModule):
    """
    Adapter class that turns an NGC into a trainable module that can be trained using Lightning.Trainer.
    Uses LME.TrainableModule abstraction and LME.TrainSetup() that configures the required propertyies as needed.
    TODO: we don't handle edge optimizer schedulers, nor graph/edge/node (?) callbacks.
    TODO: metrics are still messy. We can only have node metrics right now. We should support train_cfg metrics too
    """

    def __init__(self, base_graph: NGC, train_cfg: Dict):
        super().__init__()
        self.base_graph = base_graph
        self.train_cfg = train_cfg
        self._trainable_edges: List[NGCEdge] = None
        self._trainable_nodes: List[NGCNode] = None
        assert len(self.trainable_edges) > 0, "No trainable edges were provided"

        self._node_criteria: Dict[str, Callable] = self._build_node_criteria()
        self._edge_optimizers: Dict[str, optim.Optimizer] = self._build_edge_optimizers()
        self._graph_metrics: Dict[str, CoreMetric] = self._build_graph_metrics()

    @property
    def trainable_nodes(self) -> List[NGCNode]:
        """The list of all trainable nodes, derived from self.trainable_edges"""
        if self._trainable_nodes is None:
            self._trainable_nodes = list(set(e.output_node for e in self.trainable_edges))
        return self._trainable_nodes

    @property
    def trainable_edges(self) -> List[NGCEdge]:
        """The list of all trainable edges. A trainable edge has at least one trainable param (requries grad)"""
        if self._trainable_edges is None:
            self._trainable_edges = [edge for edge in self.base_graph.parametric_edges
                                     if LME(edge).trainable_params > 0]
        return self._trainable_edges

    @property
    @overrides
    def optimizer(self) -> List[optim.Optimizer]:
        return list(self._edge_optimizers.values())

    @property
    @overrides
    def criterion_fn(self) -> Callable:
        return self._loss_fn

    @property
    @overrides
    def metrics(self) -> Dict[str, CoreMetric]:
        return self._graph_metrics

    @property
    @overrides
    def callbacks(self) -> List[Callback]:
        prefix = "val_" if self.trainer.enable_validation else ""
        callbacks = [
            RemovePreviousTrainArtifacts(),
            # TODO: see checkpoint_monitors
            *[ModelCheckpoint(save_last=True, save_top_k=1, monitor=f"{prefix}{monitor}")
              for monitor in self.checkpoint_monitors],
            PlotMetrics(),
            PlotGraph(),
            CopyBestCheckpoint(),
        ]
        return callbacks

    @property
    @overrides
    def scheduler_dict(self) -> Dict:
        return None

    @property
    @overrides
    def checkpoint_monitors(self) -> List[str]:
        # TODO: perhaps add this information in the config file if we want to monitor multiple metrics.
        # must alingn with CopyBestCheckpoint though.
        return ["loss"]

    # Internal builder functions for the properties above, based on the train cfg and edges properties

    # Nodes criteria & graph loss function
    def _loss_fn(self, y: tr.Tensor, gt: tr.Tensor) -> tr.Tensor:
        """by default, the graph loss is the mean of all the nodes' loss optimized in this graph"""
        res = []
        for node in self.trainable_nodes:
            fn = self._node_criteria[node]
            res.append(fn(y[node], gt[node]))
        return sum(res) / len(res)

    def _build_node_criteria(self) -> Dict[str, Callable]:
        """Builds one criterion for each output node in this trainable ngc"""
        res = {}
        for node in self.trainable_nodes:
            assert node.criterion_fn is not None, f"Node '{node.name}' has no criterion fn defined. Fix it to train"
            res[node.name] = node.criterion_fn
        return res

    # Edges optimizer
    def _build_edge_optimizers(self) -> Dict[str, optim.Optimizer]:
        """Builds one optimizer for each trainable edge in this trainable ngc. Uses LME.TrainSetup for parsing"""
        assert "optimizer" in self.train_cfg, "Optimizer is not provided in the train cfg"
        optimizer_type, optimizer_args = TrainSetup.parse_optimizer(self.train_cfg["optimizer"])
        return {edge.name: optimizer_type(edge.parameters(), **optimizer_args) for edge in self.trainable_edges}

    # Graph metrics
    @staticmethod
    def _node_metric_fn(y: Dict[str, tr.Tensor], gt: Dict[str, tr.Tensor],
                        metric_fn: CoreMetric, node: NGCNode) -> tr.Tensor:
        """Adapter method that calls the metric only using the y and gt of a specific node"""
        return metric_fn(y[node], gt[node])

    def _build_graph_metrics(self) -> Dict[str, CoreMetric]:
        # handle metrics
        metrics: Dict[str, CoreMetric] = {
            "loss": CallableCoreMetric(self.criterion_fn, higher_is_better=False, requires_grad=True)
        }
        for node in self.trainable_nodes:
            if len(node.metrics) == 0:
                logger.debug(f"Node '{node.name}' has no metrics. Skipping.")

            for metric_name, metric_fn in node.metrics.items():
                assert isinstance(metric_fn, tuple) and isinstance(metric_fn[0], Callable), f"Got '{type(metric_fn)}'"
                fn = partial(TrainableNGC._node_metric_fn, metric_fn=metric_fn[0], node=node)
                assert metric_fn[1] in ("min", "max"), metric_fn
                metrics[f"{node}_{metric_name}"] = CallableCoreMetric(fn, higher_is_better=metric_fn[1] == "max")
        return metrics

    # pylint: disable=unused-argument
    def forward(self, *args, **kwargs):
        """
        Forward call to the actual graph. We need to put everything in 'x' as this is what this version of nwgraph
        expects.
        """
        return self.base_graph.forward(*args, **kwargs)
