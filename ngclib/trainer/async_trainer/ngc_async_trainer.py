"""NGC Iteration trainer module"""
from copy import deepcopy
from pathlib import Path
from overrides import overrides
from lightning_module_enhanced import LME
from lightning_module_enhanced.utils import accelerator_params_from_module
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import CSVLogger
from torch.utils.data import DataLoader

from .sequential import sequential_async_train
from .parallel import parallel_async_train
from ..ngc_trainer import NGCTrainer
from ..trainable_ngc import TrainableNGC
from ..utils import is_edge_trained, ngc_trainer_dataloader_params, \
    is_edge_partially_trained, get_edge_last_weights_file
from ...models import NGC, NGCEdge
from ...logger import logger

class NGCAsyncTrainer(NGCTrainer):
    """
    NGCAsyncTrainer implementation. Trains each edge at a time w/o any direct relationship between each other. Some
    edges may have dependencies (two hops or ensembles etc.). In this case the dependencies must be trained and are
    frozen during training of the dependent edge.

    Parameters:
        graph The full ngc graph
        train_cfg The config file used to train the NGC. Uses LME semantics.
        train_reader The train reader
        validation_reader The validation reader
        iter_dir The ngcdir/iterX/models directory where this iteration's edges are stored
        dataloader_params The parameters passed to the dataloader (no collate_fn, generator or worker_init)
        trainer_params The parameters passed to the lighting trainer
        seed The seed used for the trainer
        strategy Serial or Parallel strategies for training the edges asynchronous.
    """
    def __init__(self, *args, strategy: str, **kwargs):
        assert strategy in ("sequential", "parallel"), f"Wrong strategy: '{strategy}'"
        super().__init__(*args, **kwargs)
        if strategy == "parallel":
            # this bugs/hangs the parallel execution for some reason.
            self.dataloader_params = deepcopy(self.dataloader_params)
            self.dataloader_params["num_workers"] = 0
        self.strategy = strategy

    def train_one_edge(self, edge: NGCEdge) -> bool:
        """The main function. Trains the edge if not already trained. If partially trained, loads it first"""
        last_edge_name = self.subgraphs[edge.name].edges[-1].name
        assert last_edge_name == edge.name, "Edge must be the last one in the subgraph. " \
                                            f"Last: {last_edge_name}. Expected: {edge.name}"
        accelerator_params = accelerator_params_from_module(edge)
        edge.to("cpu")
        if is_edge_trained(edge, self.iter_dir):
            logger.debug(f"Edge '{edge}' already trained")
            return True
        if not super().are_dependencies_trained(edge):
            logger.debug(f"Not all dependencies of '{edge}' already trained")
            return False

        subgraph = deepcopy(self.subgraphs[edge.name])
        self._set_subgraph_train_params(subgraph, edge)
        self._load_all_dependencies_weights(subgraph, edge)

        subgraph_train_reader = self.train_reader.subreader(subgraph.nodes)
        subgraph_validation_reader = self.validation_reader.subreader(subgraph.nodes)
        train_dl_params = ngc_trainer_dataloader_params(subgraph_train_reader, self.dataloader_params, self.seed)
        validation_dl_params = {**train_dl_params, "shuffle": False}
        train_loader = DataLoader(subgraph_train_reader, **train_dl_params)
        validation_loader = DataLoader(subgraph_validation_reader, **validation_dl_params)

        trainable_ngc = LME(TrainableNGC(subgraph, self.train_cfg))
        edge = subgraph.edges[-1]
        ckpt_path = self._get_ckpt_path(edge)
        pl_logger = CSVLogger(save_dir=self.iter_dir, name=str(edge), version="")
        trainer = Trainer(logger=pl_logger, default_root_dir=self.iter_dir, devices=accelerator_params["devices"],
                          accelerator=accelerator_params["accelerator"], **self.trainer_params)
        trainer.fit(trainable_ngc, train_dataloaders=train_loader,
                    val_dataloaders=validation_loader, ckpt_path=ckpt_path)
        subgraph.to("cpu")
        del subgraph
        return True

    def _get_ckpt_path(self, edge: NGCEdge) -> Path:
        ckpt_path = None
        if is_edge_partially_trained(edge, self.iter_dir):
            logger.debug(f"Edge '{edge}' is partially trained, resuming from checkpoint.")
            ckpt_path = get_edge_last_weights_file(edge, self.iter_dir)
        return ckpt_path

    def _load_all_dependencies_weights(self, subgraph: NGC, edge: NGCEdge):
        """For a subgraph, load all edges, except the given edge"""
        # Load all n-1 edges, if they have params
        dep_edge: NGCEdge
        for dep_edge in subgraph.edges:
            if dep_edge.name == edge.name:
                continue
            if len(tuple(dep_edge.parameters())) == 0:
                continue
            dep_weight_file = self.iter_dir / str(dep_edge) / "checkpoints/model_best.ckpt"
            dep_edge.load_weights(dep_weight_file)
            dep_edge.requires_grad_(False)
        (self.iter_dir / str(edge)).mkdir(exist_ok=True, parents=True)

    def _set_subgraph_train_params(self, subgraph: NGC, edge: NGCEdge):
        """Set the parameters to untrainable for all the other edges, as we train each edge independently in NGC"""
        subgraph_edge: NGCEdge
        for subgraph_edge in subgraph.edges:
            if edge.name != subgraph_edge.name:
                LME(subgraph_edge).trainable_params = False

    @overrides
    def run(self):
        if self.strategy == "sequential":
            sequential_async_train(self)
        else:
            parallel_async_train(self)

    def __call__(self):
        return self.run()
