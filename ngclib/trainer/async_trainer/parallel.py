"""Parallel iteration async trainer module"""
from functools import partial
from multiprocessing import cpu_count
import torch as tr
from pool_resources import PoolResources, TorchResource
from ...logger import logger
from ...models import NGCEdge
from ..utils import is_edge_trained

def _train_one_edge(edge: NGCEdge, ngc_trainer: "NGCAsyncTrainer"):
    res = ngc_trainer.train_one_edge(edge)
    if res is False:
        deps_trained = {k.name: is_edge_trained(k, ngc_trainer.iter_dir) for k in ngc_trainer.get_dependencies(edge)}

        # Raise exception os the pool knows this edge is not done yet and will put it back in the untrained list
        all_deps_trained = sum(deps_trained.values()) == len(deps_trained)
        if not all_deps_trained:
            raise ValueError(f"Edge '{edge}' could not be trained. Not all deps trained: {deps_trained}")
        raise ValueError(f"Edge '{edge}' could not be trained. All deps trained. Must investigate. {deps_trained}")
    return True

def parallel_async_train(ngc_trainer: "NGCAsyncTrainer"):
    """parallel strategy implementation of NGCAsyncTrainer"""
    num_gpus = tr.cuda.device_count()
    num_subgraphs = len(ngc_trainer.subgraphs)
    if num_gpus == 0:
        logger.debug(f"No GPUs were found. Training {num_subgraphs} in parallel on CPUs.")
        # use all cpus if the number of subgraphs is less
        num_cpus = min(num_subgraphs, cpu_count())
        resources = [TorchResource(f"cpu:{i}") for i in range(num_cpus)]
    else:
        logger.debug(f"{num_gpus} GPUs found. Training {num_subgraphs} in parallel on GPUs.")
        resources = [TorchResource(f"cuda:{i}") for i in range(num_gpus)]
    pool = PoolResources(resources, timeout=5, pbar=False)
    while not ngc_trainer.are_all_edges_trained():
        pool.map(partial(_train_one_edge, ngc_trainer=ngc_trainer), ngc_trainer.untrained_edges)
