"""Readers init file"""
from .ngc_npz_reader import NGCNpzReader
from .clone_dim_reader import CloneDimReader
from .edge_reader import EdgeReader
from .graph_reader import GraphReader
from .utils import reader_to_data_loader
