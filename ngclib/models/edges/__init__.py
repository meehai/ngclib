"""Init file"""
from .single_link import SingleLink
from .two_hop_link import TwoHopLink
from .hyper_edge import HyperEdge
from .ensemble_edge import EnsembleEdge
