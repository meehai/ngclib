"""Single link module"""
from ..ngc_edge import NGCEdge

class SingleLink(NGCEdge):
    """Single link definition"""
