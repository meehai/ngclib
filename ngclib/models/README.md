# NGCLib Models

Here we define the basic models for the ngclib.

The main module (NGC) is implemented under ngc.py. The other ones (Single links and two hop links) are implemented under their respective submodules between all possible nodes.
