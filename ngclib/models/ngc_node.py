"""NGCNode module"""
from typing import Callable, Dict
from nwgraph.node import Node
from lightning_module_enhanced.metrics import CoreMetric
import torch as tr
import numpy as np
from ..logger import logger

class NGCNode(Node):
    """Wrapper on top of Nodes, so we can define the save/load/plot_fn methods, which are not related to nwgraph"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._metrics: Dict[str, CoreMetric] = {}
        self._criterion_fn: Callable[[tr.Tensor, tr.Tensor], tr.Tensor] = None

    def save_to_disk(self, x: np.ndarray) -> np.ndarray:
        """Given a prediction of this node, define how it should be stored on the disk"""
        raise NotImplementedError("Must be implemented")

    def load_from_disk(self, x: np.ndarray) -> np.ndarray:
        """Given a stored prediction of this node, loaded from disk, convert it to the internal representation"""
        raise NotImplementedError("Must be implemented")

    @property
    def num_channels(self) -> int:
        """
        NGCNodes work with num_channels, which is usually just self.dims[-1], but not necessarily
        We'll have to think of more complicated cases, if we ever encounter them.
        """
        assert len(self.dims) == 1, f"Expected nodes to be 1-D. Update this for your specific N-D node: {self.dims}"
        return self.dims[-1]

    @property
    def criterion_fn(self) -> Callable[[tr.Tensor, tr.Tensor], tr.Tensor]:
        """Criterion of this NGC Node. Defaults to no metrics, since it's optional for training. Updated as needed"""
        return self._criterion_fn

    @criterion_fn.setter
    def criterion_fn(self, criterion_fn: Callable[[tr.Tensor, tr.Tensor], tr.Tensor]):
        assert isinstance(criterion_fn, Callable), f"Expected callable, got '{type(criterion_fn)}'"
        self._criterion_fn = criterion_fn

    @property
    def metrics(self) -> Dict[str, CoreMetric]:
        """Metrics of this NGC Node. Defaults to no metrics, since it's optional for training. Updated as needed"""
        return self._metrics

    @metrics.setter
    def metrics(self, metrics: Dict[str, CoreMetric]):
        assert isinstance(metrics, dict), f"Expected a dict of {{str: CoreMtric}}, got '{type(metrics)}'"
        for _, metric_fn in metrics.items():
            assert isinstance(metric_fn, Callable), f"Expected a dict of {{str: CoreMtric}}, got '{type(metrics)}'"
        self._metrics = metrics

    def plot_fn(self, x: np.ndarray) -> np.ndarray:
        """Given a prediction of this node, convert it to a RGB image as numpy array"""
        logger.warning(f"Node '{self.name}' has the default plot_fn, which otuputs a black image. Overwrite it!")
        assert len(x.shape) == 3
        return np.zeros((x.shape[0], x.shape[1], 3), dtype=np.uint8)
