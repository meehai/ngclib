"""NGC Graph Module"""
from __future__ import annotations
from typing import List, Dict
from pathlib import Path
from nwgraph import Edge, Node
from nwgraph.graph import SimpleGraph

from .ngc_node import NGCNode
from .ngc_edge import NGCEdge
from ..utils import VoteFn, ConfigEdges, NGCModelFns
from ..logger import logger

class NGC(SimpleGraph):
    """
    NGC base class.
    The contructor receives a list of edges, a vote function and a list of input node names.
    Parameters:
    - edges: A list of NGC Edges properly instantiated
    - vote_fn: A voting function which is called at the aggregate step
    - input_node_names: A list of nodes. Output nodes are deduced as the difference between all nodes and this.
    """
    def __init__(self, edges: List[NGCEdge], vote_fn: VoteFn, input_node_names: List[str]):
        edges = NGC._fix_ngc_edges(edges)
        super().__init__(edges)
        self.input_node_names = NGC._fix_input_node_names(self._nodes, input_node_names)
        self.vote_fn = vote_fn
        self._ngc_subgraphs: Dict[str, NGC] = None
        self._parametric_edges: List[Edge] = None

    @staticmethod
    def edge_name_from_cfg_str(edge_type: str, cfg_edge_name: List[str], node_names: List[str],
                               input_node_names: List[str]) -> str:
        """Returns the edge name given a string from the graph cfg"""
        raise NotImplementedError("This method must be implemented by subtypes of NGC building from a graph cfg.")

    @staticmethod
    def build_graph_from_edges_cfg(nodes: List[NGCNode], edges_cfg: ConfigEdges, ngc_edge_fn: NGCModelFns,
                                   vote_fn: VoteFn, input_node_names: List[str], *args, **kwargs) -> NGC:
        """
        Builds the edges for this ngc architecture given a config and a list of nodes.
        Parameters:
        - nodes: The list of NGCNodes that are used to build the edges.
        - edges_cfg: The dictionary of edges and their types. These semantic are handled by the ngc architecture.
        - ngc_edge_fn: The callback used to instantiate the NGCEdges' models
        - vote_fn: A voting function which is called at the aggregate step
        - input_node_names: A list of nodes. Output nodes are deduced as the difference between all nodes and this.
        - rest of the arguments are passed to the NGC constructor.
        """
        raise NotImplementedError("This method must be implemented by subtypes of NGC building from a graph cfg.")

    @property
    def num_iterations(self) -> int:
        """The number of iterations, must be updated by each ngc architecture"""
        # TODO: perhaps remove and use forward properly as in nwgraph
        raise NotImplementedError("This method must be implemented by subtypes of NGC.")

    @property
    def input_nodes(self) -> List[NGCNode]:
        """The input nodes of this graph"""
        return [self.name_to_node[node_name] for node_name in self.input_node_names]

    @property
    def output_nodes(self) -> List[NGCNode]:
        """The output nodes of this graph"""
        return list(set(self._nodes).difference(self.input_nodes))

    @property
    def nodes(self) -> List[NGCNode]:
        return [*self.input_nodes, *self.output_nodes]

    @property
    def ngc_subgraphs(self) -> Dict[str, NGC]:
        """Builds the subgraphs required for this graph by doing a pass such that messages reach to that edge"""
        if self._ngc_subgraphs is None:
            self._ngc_subgraphs = super().edge_subgraphs(self.input_node_names, self.num_iterations)
        return self._ngc_subgraphs

    @property
    def parametric_edges(self) -> List[Edge]:
        """A list of all parametric edges: with params, not necessary requiring grad"""
        if self._parametric_edges is None:
            self._parametric_edges = [edge for edge in self.edges if len(tuple(edge.parameters())) > 0]
        return self._parametric_edges

    def forward(self, x, num_iterations=None):
        return super().forward(x, self.num_iterations)

    def load_all_weights(self, weights_dir: Path, mode: str = "best"):
        """Loads the weights of each edge from a previously training ngc dir"""
        assert weights_dir.stem == "models", f"Weights dir '{weights_dir}' is not called 'models'. This is a mistake"
        logger.info(f"Loading all weights ({len(self.parametric_edges)} trainable edges) from NGC weights "
                    f"dir: '{weights_dir}'")
        edge: NGCEdge
        for edge in self.parametric_edges:
            stem = "model_best.ckpt" if mode == "best" else "last.ckpt"
            edge_weights_file = weights_dir / str(edge) / "checkpoints" / stem
            edge.load_weights(edge_weights_file)

    @staticmethod
    def _fix_ngc_edges(edges: List[Edge]) -> List[NGCEdge]:
        """If regular edges are given, convert to ngc edges. For compatibility purposes."""
        new_edges = []
        for edge in edges:
            if len(tuple(edge.parameters())) > 0 and not isinstance(edge, NGCEdge):
                logger.warning(f"Edge '{edge}' is not an instance of NGCEdge. Trying to convert!")
                edge = NGCEdge.build_from_edge(edge)
            new_edges.append(edge)
        assert len(edges) == len(new_edges), f"Before: {len(edges)}. After: {len(new_edges)}"
        return new_edges

    @staticmethod
    def _fix_input_node_names(nodes: List[Node], input_node_names: List[str]) -> List[str]:
        """Removes duplicate or inexistent names, if they exist"""
        if len(set(input_node_names).intersection(nodes)) != len(input_node_names):
            logger.warning(f"{input_node_names} vs {[x.name for x in nodes]}. Removing additional input nodes.")
            input_node_names = list(set(input_node_names).intersection(nodes))
        return input_node_names
