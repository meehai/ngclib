"""Dump predictions from a (preferably pre-trained) NGC"""
from pathlib import Path
from typing import List, Tuple, Dict
import os
import torch as tr
import numpy as np
from nwgraph import Graph
from tqdm import tqdm
from PIL import Image

from torch.utils.data import DataLoader

from ..logger import logger
from .torch import to_device

class DumpDiskWriter:
    """
    Wrapper on top of np.save/image_write which checks in a cache if the same numpy array was previously stored, and if
    so, then it will just make a symlink towards that previous save. Super useful for shitty nodes with a lot of
    redundancies (H2 edges with t=4 making copies of CatNodes).
    """
    def __init__(self):
        # self.cache = DictMemory(name="ngc_cache", key_encode_fn=DumpDiskWriter._cache_fn, clash_exception=True)
        self.cache = {}

    def __call__(self, node: "NGCNode", out_dir: Path, trs: List[tr.Tensor], names: List[str], mode: str):
        out_dir.mkdir(exist_ok=True, parents=True)
        fn_transform = DumpDiskWriter._get_transform_fn(node, mode)
        if fn_transform is None:
            return

        for i, item in enumerate(trs):
            y_np = item.to("cpu").detach().numpy()
            y_transformed = fn_transform(y_np)
            out_file = Path(f"{out_dir}/{names[i]}.npz" if mode == "npy" else f"{out_dir}/{names[i]}.png")
            self._write_to_disk(y_transformed, node, mode, out_file)

    @staticmethod
    def _key_encode_fn(node: "NGCNode", item: np.ndarray) -> Tuple[str, float, float]:
        x: np.ndarray = item[item != 0].astype(np.float32)
        mean, std = x.mean().item(), x.std().item()
        return node, mean, std

    @staticmethod
    def _get_transform_fn(node: "NGCNode", mode: str):
        assert mode in ("png", "npy")
        if mode == "npy":
            return node.save_to_disk
        return node.plot_fn

    def _write_to_disk(self, item: np.ndarray, node: str, mode: str, out_path: Path):
        key = DumpDiskWriter._key_encode_fn(node, item)
        try:
            cache_path = Path(self.cache[key])
            assert cache_path.exists() and cache_path.is_file() and not cache_path.is_symlink(), cache_path
            if not out_path.exists():
                rel_cache_path = Path(os.path.relpath(cache_path.parent, out_path.parent)) / cache_path.name
                assert rel_cache_path != out_path
                os.symlink(rel_cache_path, out_path)
            return
        except KeyError:
            self.cache[key] = out_path

        if mode == "npy":
            np.savez(out_path, item)
        else:
            Image.fromarray(item).save(out_path)

def dump_predictions(graph: Graph, dataloader: DataLoader, output_nodes: List["NGCNode"],
                     output_path: Path, dump_png: bool):
    """dump predictions given a NGC graph, a dataloader, a set of output nodes and a path"""

    device = next(graph.parameters()).device
    disk_writer = DumpDiskWriter()
    for data in tqdm(iter(dataloader)):
        disk_writer.cache.clear()
        x, gt = to_device(data["data"], device), data["labels"]
        names = data["name"]
        collected_items: Dict["NGCNode", Dict[str, tr.Tensor]] = \
            to_device(graph.collect_states_and_messages(x, graph.num_iterations), "cpu")

        # add gt data to output nodes as well
        for node in output_nodes:
            if node in gt:
                collected_items[node]["state_-1"] = gt[node]
            else:
                logger.warning(f"Node '{node}' has no gt for this batch. Only outputting messages and states.")

        for node in graph.nodes:
            for k, v in collected_items[node.name].items():
                timestamp = "gt" if k.split("_")[-1] == "-1" else k.split("_")[-1]
                dir_name = "_".join(k.split("_")[0: -1])
                out_dir = output_path / node.name / timestamp / dir_name / "npy"
                disk_writer(node, out_dir, v, names, "npy")

                if dump_png:
                    _v = v * (gt[node.name] > 0) if node in output_nodes else v
                    out_dir = output_path / node.name / timestamp / dir_name / "png"
                    disk_writer(node, out_dir, _v, names, "png")

    logger.info(f"Dump dir saved at: '{output_path}'")
