"""NGCNode Imported module"""
from typing import List
from overrides import overrides
from nwgraph.utils import NodesImporter

from .graph_cfg import GraphCfg
from ..models import NGCNode

class NGCNodesImporter(NodesImporter):
    """Wrapper on top of NodesImporter to support loading from a GraphCfg"""
    @classmethod
    def from_graph_cfg(cls, graph_cfg: GraphCfg, *args, **kwargs) -> NodesImporter:
        """Class method to construct from graph_cfg."""
        return cls(graph_cfg.node_names, graph_cfg.node_types, graph_cfg.node_args, *args, **kwargs)

    @property
    @overrides(check_signature=False)
    def nodes(self) -> List[NGCNode]:
        return super().nodes
