"""Graph Cfg module"""
from copy import deepcopy
from typing import Dict, List, Type, Optional

from ..models import NGC, NGCNode, build_model_type
from ..utils import ConfigEdges, VoteFn, NGCModelFns
from ..logger import logger


class GraphCfg:
    """
    Graph Cfg implementation. Nodes and edges are only strings/literals, no instantiations!
    Use NGCNodesImporter() to instantiate nodes.
    """

    def __init__(self, graph_cfg: Dict, ngc_type: Optional[Type] = None):
        assert isinstance(graph_cfg, dict), f"Expected dict, got {type(graph_cfg)}"
        self.cfg = deepcopy(graph_cfg)
        self._validate_cfg()
        if ngc_type is not None:
            assert self.cfg.get("NGC-Architecture") is None, "Cannot provide it both manually and in cfg!"
            self.ngc_type = ngc_type
        else:
            # build from a built-in ngclib architecture (ngc-v1, ngc-hyperedges etc.)
            self.ngc_type = build_model_type(self.cfg["NGC-Architecture"])
        self.seed = int(self.cfg["seed"]) if "seed" in self.cfg else 42
        self.edges_raw: ConfigEdges = self.cfg["edges"]

        self._node_names: List[str] = None
        self._node_types: List[str] = None
        self._input_nodes: List[str] = None
        self._output_nodes: List[str] = None
        self._edges: List[str] = None
        self._edge_name_to_edge_raw: Dict[str, str] = None
        self.node_args: Dict[str, Dict] = self._get_nodes_args()
        logger.info(f"Architecture: {self.ngc_type}. Nodes: {len(self.node_names)}. Edges: {len(self.edges)}")

    @property
    def node_names(self) -> List[str]:
        """The list of node names. Properly sorted as well."""
        if self._node_names is None:
            self._node_names = self.cfg["nodes"]["names"]
        return self._node_names

    @property
    def node_types(self) -> List[str]:
        """The list of node types. Properly sorted w.r.t node names as well."""
        if self._node_types is None:
            self._node_types = self.cfg["nodes"]["types"]
        return self._node_types

    @property
    def input_nodes(self) -> List[str]:
        """List of inputs nodes (names), sorted"""
        if self._input_nodes is None:
            self._input_nodes = sorted(list(set(self.cfg["nodes"]["inputNodes"]).intersection(self.node_names)))
        return self._input_nodes

    @property
    def output_nodes(self) -> List[str]:
        """List of output nodes (names), sorted"""
        if self._output_nodes is None:
            self._output_nodes = sorted(list(set(self.node_names).difference(self.input_nodes)))
        return self._output_nodes

    @property
    def edges(self) -> List[str]:
        """Converts the edges raw strings to the names of the edges w/o instantiating them using the ngc type"""
        if self._edges is None:
            self._edges = []
            for edge_type, edges in self.edges_raw.items():
                for edge in edges:
                    edge_name = self.ngc_type.edge_name_from_cfg_str(edge_type, edge, self.node_names,
                                                                     self.input_nodes)
                    self._edges.append(edge_name)
        return self._edges

    @property
    def edge_name_to_edge_raw(self) -> Dict[str, str]:
        """A mapping based on self.edges between cfg strings and edge names in NGC"""
        if self._edge_name_to_edge_raw is None:
            self._edge_name_to_edge_raw = {}
            for edge_type, edges in self.edges_raw.items():
                for edge in edges:
                    edge_name = self.ngc_type.edge_name_from_cfg_str(edge_type, edge, self.node_names,
                                                                     self.input_nodes)
                    self._edge_name_to_edge_raw[edge_name] = edge
        return self._edge_name_to_edge_raw

    def build_model(self, nodes: List[NGCNode], ngc_edge_fn: NGCModelFns,
                    vote_fn: VoteFn, *args, **kwargs) -> NGC:
        """
        Builds a model using the ngc type and edges provided to this cfg.
        To get the nodes, use NodeImporter or build them manually.

        Parameters:
        - nodes: A list of instantiated NGC nodes
        - ngc_edge_fn: A callback used to instantiate each edge's NN model of the graph.
        - vote_fn: The voting function used to aggregate the data at NGC level
            (TODO: perhaps we could rethink this a bit to be more flexible)

        Returns: A NGC instance of the type provided in the graph cfg under 'NGC-Architecture'.
        """
        return self.ngc_type.build_graph_from_edges_cfg(nodes=nodes, edges_cfg=self.edges_raw, ngc_edge_fn=ngc_edge_fn,
                                                        vote_fn=vote_fn, input_node_names=self.input_nodes,
                                                        *args, **kwargs)

    def _validate_cfg(self):
        """Basic consistency validation"""
        if "edges" not in self.cfg:
            logger.warning("No edegs in graph cfg")
            self.cfg["edges"] = {}
        assert "nodes" in self.cfg, f"'nodes' key not in the graph cfg: {self.cfg}"
        assert "types" in self.cfg["nodes"], f"No node types in graph cfg. Most likely an error. {self.cfg}"
        assert "names" in self.cfg["nodes"], f"No node names in graph cfg. Most likely an error. {self.cfg}"
        assert "inputNodes" in self.cfg["nodes"], "No input nodes in graph cfg."

        # nodes
        node_names, node_types = self.cfg["nodes"]["names"], self.cfg["nodes"]["types"]
        assert len(node_names) > 0, "At least one node must exist"
        assert len(node_names) == len(node_types), f"Names and types mismatch: {len(node_names)} vs {len(node_types)}"
        assert len(self.cfg["nodes"]["inputNodes"]) > 0, "No input nodes in graph cfg"

        # node hparams
        if "hyperParameters" not in self.cfg or self.cfg["hyperParameters"] is None:
            self.cfg["hyperParameters"] = {}
        for node in self.cfg["hyperParameters"]:
            assert node in node_names, f"Node '{node}' has hparams, but is not in node names: {node_names}"

        # edges
        assert isinstance(self.cfg["edges"], dict), "edges must be a dict with edge types as keys and the edges as " \
                                                    f"a list of values, got {self.cfg['edges']}"
        for k, v in self.cfg["edges"].items():
            assert isinstance(v, list), f"For key '{k}' expected a list of edges, got {v}"
            for edge in v:
                for node in edge:
                    # [ *, out ]
                    if node == "*":
                        continue
                    # [ [a,b,c], out ]
                    if isinstance(node, str):
                        node = [node]
                    for _node in node:
                        assert isinstance(_node, str), (_node, type(_node), node, edge)
                        assert _node in node_names, (_node, node_names)

    def _get_nodes_args(self) -> Dict[str, Dict]:
        """Gets the arguments for each node, given its name. If no argument, an empty dict is set"""
        if not "hyperParameters" in self.cfg:
            return {node_name: {} for node_name in self.node_names}
        hparams = self.cfg["hyperParameters"]
        res = {}
        for node_name in self.node_names:
            if node_name not in hparams:
                res[node_name] = {}
            else:
                res[node_name] = hparams[node_name]
        return res

    def _setup_node_names(self):
        """Gets the names and types given the graph cfg and checks if any edges are left redundancy shrinking"""
        node_names, node_types = self.cfg["nodes"]["names"], self.cfg["nodes"]["types"]
        return node_names, node_types

    def __str__(self):
        f_str = f"""[Graph Cfg]
 - NGC Type: {self.ngc_type}
 - Node types: {self.node_types}
 - Node names: {self.node_names}
 - Input nodes: {self.input_nodes}
 - Output nodes: {self.output_nodes}
"""
        return f_str

    def __repr__(self):
        return str(self)
