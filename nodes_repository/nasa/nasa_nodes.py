"""NASA nodes implementation in ngclib repository"""
# pylint: disable=missing-class-docstring, multiple-statements
from pathlib import Path
from codecs import encode
from overrides import overrides
import numpy as np
from ngclib.models import NGCNode

def _cmap_hex_to_rgb(hex_list):
    res = []
    for hex_data in hex_list:
        r = int(hex_data[1: 3], 16)
        g = int(hex_data[3: 5], 16)
        b = int(hex_data[5: 7], 16)
        res.append([r,g,b])
    return np.array(res)


def _act_to_cmap(act_file):
    with open(act_file, "rb") as act:
        raw_data = act.read()                           # Read binary data
    hex_data = encode(raw_data, "hex")                  # Convert it to hexadecimal values
    total_colors_count = int(hex_data[-7:-4], 16)     # Get last 3 digits to get number of colors total
    total_colors_count = 256

    # Decode colors from hex to string and split it by 6 (because colors are #1c1c1c)
    colors = [hex_data[i:i+6].decode() for i in range(0, total_colors_count*6, 6)]

    # Add # to each item and filter empty items if there is a corrupted total_colors_count bit
    hex_colors = ["#"+i for i in colors if len(i)]
    rgb_colors = _cmap_hex_to_rgb(hex_colors)
    return rgb_colors

class NASANode(NGCNode):
    """NASA nodes implementation in ngclib repository"""
    def __init__(self, name: str):
        # all nasa nodes have 1 dimension.
        super().__init__(name, dims=(1,))
        self._cmap = None

    @staticmethod
    def _default_transform(x: np.ndarray) -> np.ndarray:
        x: np.ndarray = np.float32(x)
        if x.shape[0] == 1: # pylint: disable=unsubscriptable-object
            x = x[0] # pylint: disable=unsubscriptable-object
        if len(x.shape) == 2:
            x = np.expand_dims(x, axis=-1)
        return x

    def loss_fn(self, y, gt):
        """The criterion of all nasa nodes: l2 loss masked"""
        mask = gt != 0
        y = y * mask
        loss = (y - gt).pow(2).sum() / mask.sum()
        return loss

    @property
    def criterion_fn(self):
        """The criterion of all nasa nodes"""
        return self.loss_fn

    def l1_masked(self, y, gt):
        """l1_masked metric"""
        y = y.reshape(y.shape[0], -1)
        gt = gt.reshape(gt.shape[0], -1)
        mask = gt != 0
        y = y * mask
        res_l1_masked = list((y - gt).abs().sum(axis=1) / mask.sum(axis=1))
        return res_l1_masked

    def mse_masked(self, y, gt):
        """mse_masked metric"""
        y = y.reshape(y.shape[0], -1)
        gt = gt.reshape(gt.shape[0], -1)
        mask = gt != 0
        y = y * mask
        res_mse_masked = list((y - gt).pow(2).sum(axis=1) / mask.sum(axis=1))
        return res_mse_masked

    @property
    def metrics(self):
        """the list of metris for nasa nodes"""
        return {"l1_masked": (self.l1_masked, "min"), "mse_masked": (self.mse_masked, "min")}

    @property
    def cmap(self):
        """cmaps for the nasa nodes based on their type"""
        if self._cmap is None:
            str_type = str(type(self)).split(".")[-1][0:-2]
            cmap_file = Path(__file__).absolute().parent / "cmaps" / f"{str_type}.act"
            self._cmap = _act_to_cmap(cmap_file)
        return self._cmap

    @overrides
    def load_from_disk(self, x: np.ndarray) -> np.ndarray:
        y = NASANode._default_transform(x)
        y[np.isnan(y)] = 0
        return y

    @overrides
    def save_to_disk(self, x: np.ndarray) -> np.ndarray:
        return x.clip(0, 1)

    @overrides
    def plot_fn(self, x: np.ndarray) -> np.ndarray:
        y = np.clip(x, 0, 1)
        y = y * 255
        y[y==0] = 255
        y = y.astype(np.uint).squeeze()
        y_rgb = self.cmap[y].astype(np.uint8)
        return y_rgb

class AerosolOpticalDepth(NASANode): pass
class CarbonMonoxide(NASANode): pass
class Chlorophyll(NASANode): pass
class CloudFraction(NASANode): pass
class CloudOpticalThickness(NASANode): pass
class CloudParticleRadius(NASANode): pass
class CloudWaterContent(NASANode): pass
class Fire(NASANode): pass
class LeafAreaIndex(NASANode): pass
class NitrogenDioxide(NASANode): pass
class Ozone(NASANode): pass
class SeaSurfaceTemperature(NASANode): pass
class SnowCover(NASANode): pass
class Temperature(NASANode): pass
class TemperatureAnomaly(NASANode): pass
class Vegetation(NASANode): pass
class WaterVapor(NASANode): pass
