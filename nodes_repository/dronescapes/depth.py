"""Depth node definition"""
# TODO: see ngc-training code and see if/what we need to get from there.
from overrides import overrides
from media_processing_lib.image import to_image
from matplotlib.cm import get_cmap
import numpy as np
import torch as tr
from torch.nn import functional as F

from .dronescapes_node import DronescapesNode

class Depth(DronescapesNode):
    """Depth node implementation"""

    def __init__(self, name: str, max_depth_meters: float):
        super().__init__(name, dims=(1,))
        self.max_depth_meters = max_depth_meters

    @overrides
    def load_from_disk(self, x: np.ndarray) -> np.ndarray:
        """Converts from disk [0:max_meters] to [0:1] and clips it properly"""
        x = x / self.max_depth_meters
        x = np.clip(x, 0, 1).astype(np.float32)
        return super().load_from_disk(x)

    @overrides
    def save_to_disk(self, x: np.ndarray) -> np.ndarray:
        """Converts back to [0: max_depth_meters] and float16"""
        y = np.clip(x, 0, 1)
        y = (y * self.max_depth_meters).astype(np.float16)
        return y

    @overrides
    def plot_fn(self, x: np.ndarray) -> np.ndarray:
        if x.max() > 1:
            x = x / self.max_depth_meters
        y = np.clip(x, 0, 1).squeeze()
        cmap = get_cmap("plasma")
        y = cmap(y)[..., 0:3]
        y = to_image(y)
        return y

    def _loss_fn(self, y: tr.Tensor, gt: tr.Tensor) -> tr.Tensor:
        assert len(self.messages) == 1
        return F.mse_loss(y, gt).mean()

    @property
    def criterion_fn(self):
        return self._loss_fn

    @property
    def metrics(self):
        return {}
