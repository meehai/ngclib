"""Semantic node definition"""
from typing import List, Union, Tuple
from overrides import overrides
from media_processing_lib.image import to_image
import numpy as np
import torch as tr
from torch.nn import functional as F

from .dronescapes_node import DronescapesNode


class Semantic(DronescapesNode):
    """Semantic node implementation"""

    def __init__(self, name: str, semantic_classes: Union[int, List[str]],
                 semantic_colors: List[Tuple[int, int, int]], encoding: str):
        """
        Parameters:
        - name: The node of the node
        - semantic_classes: A list (or integer) of the class names
        - semantic_colors: A list of RGB colors to convert each class to a RGB image
        - encoding:
            - soft: A float32 representation of shape (H, W, C), C = num classes after softmax; last channel sums to 1
            - hard: A uint8 representation of shape (H, W) with values [0: C-1]
            - onehot: A float32 representation of shape (H, W, C) derived from the hard representation
        """
        if isinstance(semantic_classes, list):
            num_classes = len(semantic_classes)
        else:
            num_classes = semantic_classes
            semantic_classes = list(range(semantic_classes))
        super().__init__(name, dims=(num_classes,))

        assert encoding in ("soft", "hard", "onehot"), encoding
        assert len(semantic_colors) == len(semantic_classes), f"{semantic_colors} vs {semantic_classes}"
        self.semantic_classes = semantic_classes
        self.semantic_colors = semantic_colors
        self.num_classes = num_classes
        self.encoding = encoding

    @overrides
    def load_from_disk(self, x: np.ndarray) -> np.ndarray:
        """Semantic transform, returns index vector. Output shape: (H, W) with values [0 : C)"""
        if x.dtype in (np.float16, np.float32):
            if self.encoding == "soft":
                assert x.shape[-1] == self.num_classes, f"Predictions: {x.shape}. Num classes: {self.num_classes}"
                return x.astype(np.float32)
            assert self.encoding == "hard"
            x = x.argmax(axis=-1).astype(np.uint8)
        if x.dtype in (np.uint8, np.uint16, np.uint32, int):
            assert self.encoding != "soft", "Cannot convert stored hard predictions into soft probabilities"
            x = x.astype(np.uint8)
        # sometimes we get a trailing 1 in there
        x = x[..., 0] if x.shape[-1] == 1 else x
        assert x.dtype == np.uint8, x.dtype
        if self.encoding == "hard":
            # expensive check...
            # assert x.max() <= len(self.semantic_classes) - 1, f"{x.max()} vs {len(self.semantic_classes)}"
            return x

        # one-hot
        assert self.encoding == "onehot"
        return np.eye(self.num_classes, dtype=np.float32)[x]

    @overrides
    def save_to_disk(self, x: np.ndarray) -> np.ndarray:
        """Converts back to softmaxed prediction or uint8, depending on one_hot_encoding parameter"""
        # Storing is always done as soft-labels, so we preserve the predictions
        assert x.shape[-1] == self.num_classes, (x.shape, self.num_classes)
        assert x.dtype in (np.float16, np.float32), x.dtype
        return x.astype(np.float16)

    @overrides
    def plot_fn(self, x: np.ndarray) -> np.ndarray:
        n_classes = len(self.semantic_classes)
        assert n_classes == len(self.semantic_colors)
        if len(x.shape) == 3 and x.shape[-1] != 1:
            assert n_classes == x.shape[-1], f"{n_classes} vs {x.shape}"
            x = np.argmax(x, axis=-1)
        assert x.max() < n_classes

        image = np.zeros((*x.shape, 3), dtype=np.uint8)
        for i in range(n_classes):
            image[x == i] = self.semantic_colors[i]
        image = to_image(image)
        return image

    def _loss_fn(self, y: tr.Tensor, gt: tr.Tensor):
        assert len(self.messages) == 1
        y = F.softmax(y, dim=-1).reshape(-1, self.num_classes)
        gt = F.one_hot(gt.long(), num_classes=self.num_classes).float() if gt.shape[-1] != self.num_classes else gt
        gt = gt.reshape(-1, self.num_classes)
        return F.cross_entropy(y, gt).mean()

    @property
    def criterion_fn(self):
        return self._loss_fn

    @property
    def metrics(self):
        return {}
