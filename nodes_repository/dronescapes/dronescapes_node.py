"""Dronescapes node"""
from overrides import overrides
from media_processing_lib.image import to_image
import numpy as np

from ngclib.models import NGCNode

class DronescapesNode(NGCNode):
    """Dronescapes node"""
    @staticmethod
    def _default_transform(x: np.ndarray) -> np.ndarray:
        x: np.ndarray = np.float32(x)
        if x.shape[0] == 1: # pylint: disable=unsubscriptable-object
            x = x[0] # pylint: disable=unsubscriptable-object
        if len(x.shape) == 2:
            x = np.expand_dims(x, axis=-1)
        return x

    def load_from_disk(self, x: np.ndarray) -> np.ndarray:
        """Default transform just converts to float and adds a final dim if not provided. Output shape: (H, W, C)."""
        return DronescapesNode._default_transform(x)

    @overrides
    def save_to_disk(self, x: np.ndarray) -> np.ndarray:
        """Converts back normals to save on disk as float16"""
        return DronescapesNode._default_transform(x)

    def plot_fn(self, x: np.ndarray) -> np.ndarray:
        return to_image(np.clip(x, 0, 1))
