"""RGB node definition"""
from .dronescapes_node import DronescapesNode

class RGB(DronescapesNode):
    """RGB node implementation"""

    def __init__(self, name: str):
        super().__init__(name, dims=(3,))
