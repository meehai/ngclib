"""
Normal node definition
TODOs:
 - representation type: unit vector, 3 angles, cos/sin etc.
 - metrics to convert to [0 - 180] l1 angles differences
"""
import torch as tr
from torch.nn import functional as F

from .dronescapes_node import DronescapesNode

class Normals(DronescapesNode):
    """Normals node implementation"""

    _num_dims = {"euler": 3, "quaternion": 4}

    def __init__(self, name: str, representation: str):
        super().__init__(name, dims=(Normals._num_dims[representation], ))
        self.representation = representation

    def _loss_fn(self, y: tr.Tensor, gt: tr.Tensor) -> tr.Tensor:
        assert len(self.messages) == 1
        return F.mse_loss(y, gt).mean()

    @property
    def criterion_fn(self):
        return self._loss_fn

    @property
    def metrics(self):
        return {}
